import numpy as np
import matplotlib.pyplot as plt
import os
import seaborn as sns
import patron2d
import math
import numpy.random as rnd
#-------------------------------------------------------
def MinMax(D,l):
    s=int(math.log2(l//2))
    print([2**(s*D),l*l*2**(s*(D-2))])
#-------------------------------------------------------
def RandGrid(r1,r2,l):

    grid = np.zeros((l,l))
    N1 = round(r1*l**2)
    N2 = round(r2*l**2)

    choices_list = []
    choices_dict = {}
    t=0
    for i in range (l):
        for j in range(l):
            choices_list.append(t)
            choices_dict[t] = [i,j]

            t+=1
    
    dumN1 = np.copy(N1)
    dumN2 = np.copy(N2)
    
    cond1 = True
    cond2 = True
    while cond1==True or cond2==True:
        if cond1==True:
            choice1 = rnd.choice(choices_list)
            point1 = choices_dict[choice1]
            grid[point1[0],point1[1]] = 1

            choices_list.remove(choice1)
            dumN1-=1
            if dumN1 == 0:
                cond1 = False

        if cond2==True:
            choice2 = rnd.choice(choices_list)
            point2 = choices_dict[choice2]
            grid[point2[0],point2[1]] = -1

            choices_list.remove(choice2)
            dumN2-=1
            if dumN2 == 0:
                cond2 = False
        
    return grid 
#-------------------------------------------------------


exp_folder = "tmp/p23"
try: os.mkdir(exp_folder)
except: pass

l = 64
D = 1.5
D_epsilon = 0.05
m = np.log2(l/2.)

dim_maj = 1.75
dim_min = 1.45
r_maj = .15
r_min = .15

nit = 100
reds = 0
blues = 0
ties = 0
for it in range(nit):
    print(it)

    NumbersA, _ = patron2d.get_nboxes_list(r_maj, l, dim_maj, D_epsilon=D_epsilon)
    NumbersB, _ = patron2d.get_nboxes_list(r_min, l, dim_min, D_epsilon=D_epsilon)

    grid, _ = patron2d.get_two_structured(dim_maj,dim_min,l,NumbersA,NumbersB)
    for i in range(l):
        for j in range(l):
            if grid[i,j]==2: grid[i,j] = -1
        
    fig = plt.figure(figsize=(8,6))
    ax = sns.heatmap( grid ,cmap = 'coolwarm' )
    ax.set(xticklabels=[])
    ax.set(yticklabels=[])
    fig.savefig(exp_folder+"/it"+str(it)+"_grid_antes.png", dpi=200)


    Szj = patron2d.Sznajd(grid)

    for i in range(1000000):
        grid = Szj.SznajdStep(grid)
    red, blue = Szj.Count(grid)
    if red > blue: reds += 1
    elif red < blue: blues += 1
    else:   ties += 1

    fig = plt.figure(figsize=(8,6))
    ax = sns.heatmap( grid ,cmap = 'coolwarm' )
    ax.set(xticklabels=[])
    ax.set(yticklabels=[])
    fig.savefig(exp_folder+"/it"+str(it)+"_grid_despues.png", dpi=200)

print(reds, blues, ties)
