import mpmath as mp
import numpy as np
import matplotlib.pyplot as plt


def uvec(k,A,D):
    #funcion para calcular el primer argumento de la función hipergeométrica.

    return [int((2**(D-2)-1)*A*2**(-k*D)+i/4) for i in range(4)]

#------------------------------------------------------------------------------------------------------
def vvec(k,A,D):
    #funcion para calcular el segundo argumento de la función hipergeométrica.

    return [int(-A*2**(-k*D)+(i+1)/4) for i in range(3)]

#------------------------------------------------------------------------------------------------------
def F_func(k,A,D):
    #función para evaluar la función hipergeométrica generalizada

    return mp.re(mp.hyper(uvec(k,A,D),vvec(k,A,D),1,maxprec=100000))

#------------------------------------------------------------------------------------------------------
def Dmin(A,m):
    #cálculo de Dmin
    return np.amin([np.log2(A)/m,2])
    #return np.log2(A)
#------------------------------------------------------------------------------------------------------
def Dmax(A,m,L):
    #Cálculo de Dmax
    return np.amin([np.log2(A/(L**2))/m+2,2])
    #return np.log2(A/(L**2))

#------------------------------------------------------------------------------------------------------
def Bi(k,A,D):      
    #cálculo de coeficiente binomial (xy z) que acompaña a la función hipergeométrica

    return mp.binomial(4*A*2**(-k*D),A*2**(-k*D+D))

#------------------------------------------------------------------------------------------------------
def LogOmega(A,D):
    #cálculo de log(Omega) del problema 14
    L=0
    for k in range(1,5):
        try:
            L=L+mp.re(mp.log10(F_func(k,A,D)*Bi(k,A,D)))
        except:
            pass
    return L

#------------------------------------------------------------------------------------------------------
def LD(D,m):
    return 2**(m*D)

def UD(D,m,L):
    return (L**2)*2**(m*(D-2))

#------------------------------------------------------------------------------------------------------
def Entropy(D,m,L,N):
    #Funcion para calcular la entropía

    return [mp.re(LogOmega(LD(D,m)+(i/N)*(UD(D,m,L)-LD(D,m)),D)) for i in range(N+1)]
    
#------------------------------------------------------------------------------------------------------
def Shannon(t,log=10): 
    #Función de Shannon para el la aproximación de la entropía
    
    if (t<1 and t>0):
        if log==10:
            return ( -t*np.log10(t) - (1-t)*np.log10(1-t) )
        elif log==2:
            return ( -t*np.log2(t) - (1-t)*np.log2(1-t) )
    else: 
        return 0

#------------------------------------------------------------------------------------------------------
#PROBLEMA 14

L=64
D=1.5
A=round(0.15*L**2)
m=round(np.log2(L/2))

u = uvec(1,A,D)
v = vvec(1,A,D)

print("u=",u)
print("v=",v)

print("ejemplo de F:")
print("F(3,A,D)=", F_func(3,A,D) )
print("log(Omega)=", mp.log10(Bi(1,A,D)))

N = 100
A1 = int(0.07*L**2)
A2 = int(0.15*L**2)
A3 = int(0.3*L**2)
A4 = int(0.6*L**2)

dims1 = np.linspace(Dmin(A1,m),Dmax(A1,m,L),N)
dims2 = np.linspace(Dmin(A2,m),Dmax(A2,m,L),N)
dims3 = np.linspace(Dmin(A3,m),Dmax(A3,m,L),N)
dims4 = np.linspace(Dmin(A4,m),Dmax(A4,m,L),N)

LogOmega1 = np.zeros(N)
LogOmega2 = np.zeros(N)
LogOmega3 = np.zeros(N)
LogOmega4 = np.zeros(N)

for i in range(N):
    LogOmega1[i] = LogOmega(A1,dims1[i])
    LogOmega2[i] = LogOmega(A2,dims2[i])
    LogOmega3[i] = LogOmega(A3,dims3[i])
    LogOmega4[i] = LogOmega(A4,dims4[i])

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(dims1, LogOmega1, label="r = 0.07")
ax.plot(dims2, LogOmega2, label="r = 0.15")
ax.plot(dims3, LogOmega3, label="r = 0.3")
ax.plot(dims4, LogOmega4, label="r = 0.6")
ax.legend()
ax.set_ylabel(r'$\log_{10} \Omega$')
ax.set_xlabel("D")
fig.tight_layout()

fig.savefig("p_14", dpi=200)
#-----------------------------------------------------------------------------------------
#PROBLEMA 16

N=100
dims=np.linspace(0,2,N)
S_arr=np.zeros(N)
for i in range(N):
    S_arr[i]=np.amax(Entropy(dims[i],m,L,2))

fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(111)

ax.plot(dims,S_arr,label='S(D)')
ax.legend()
ax.set_ylabel('S(D)')
ax.set_xlabel("D")

#derivadas

#np.gradient(Ses)/(x[1]-x[0]

DS_arr = np.gradient(S_arr)/(dims[1]-dims[0])
D2S_arr = np.gradient(DS_arr)/(dims[1]-dims[0])

maxS=dims[np.argmax(S_arr)]
maxDS=dims[np.argmax(DS_arr)]
maxD2S=dims[np.argmax(D2S_arr)] #los máximos de las derivadas

print("diferencia entre Smax y dSmax:",dims[np.argmax(S_arr)]-dims[np.argmax(DS_arr)])
print("diferencia entre dSmax y d2Smax:",dims[np.argmax(DS_arr)]-dims[np.argmax(D2S_arr)])

fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(111)

ax.set_xlabel("D")

ax.plot(dims,S_arr, label='S(D)', color='tab:blue')
ax.vlines(maxS,0,3000, color='tab:blue', alpha=0.5)

ax.plot(dims,DS_arr , label=r'$dS/dD$', color='tab:red')
ax.vlines(maxDS,0,3000, color='tab:red', alpha=0.5)

ax.plot(dims,D2S_arr,label=r'$d^2S/dD^2$',color='orange')
ax.vlines(maxD2S,0,3000, color='tab:orange', alpha=0.5)

ax.set_ylim([0,1300])
ax.legend(loc='upper left')

fig.tight_layout()

fig.savefig("p_16", dpi=200)


#-----------------------------------------------------------------------------------------
#PROBLEMA 18


N=100
D=2 #defino la dimension del espacio
m=int(np.log2(N/2)) #defino m


x=np.linspace(0,2,N)
S_aprox=[(L/2**(m-1))**D * Shannon(2**(d-D)) * (2**(m*d)-1) / (2**d-1) for d in x] #la entropía aproximada
S_aprox[0]=(L/2**(m-1))**D * Shannon(2**(-D)) * m #tiene un error en cero, asi que ahí la defino como el límite

DS_aprox = np.gradient(S_aprox)/(x[1]-x[0])
D2S_aprox = np.gradient(DS_aprox)/(x[1]-x[0])

fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(111)
ax.plot(x,S_arr,label="Exacta")
ax.plot(x,S_aprox,label="Aprox")
ax.legend()
ax.set_ylabel('S(D)')
ax.set_xlabel("D")
fig.tight_layout()

fig.savefig("p_18a", dpi=200)


maxS_aprox=x[np.argmax(S_aprox)]
maxDS_aprox=x[np.argmax(DS_aprox)]
maxD2S_aprox=x[np.argmax(D2S_aprox)] #los máximos de sus derivadas

fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(111)
ax.set_ylim([0,1000])
ax.set_xlabel("D")

ax.plot(x,S_aprox,label=r'$S_{aprox}(D)$',color='tab:blue')
ax.vlines(maxS_aprox,0,2300, color='tab:blue', alpha = 0.5)

ax.plot(x,DS_aprox,label=r'$dS_{aprox}$',color='tab:red')
ax.vlines(maxDS_aprox,0,2300, color='tab:red', alpha = 0.5)

ax.plot(x,D2S_aprox,label=r'$d^2S_{aprox}$',color='tab:orange')
ax.vlines(maxD2S_aprox,0,2300, color='tab:orange', alpha = 0.5)

print("diferencia entre Smax y dSmax:",maxS_aprox-maxDS_aprox)
print("diferencia entre dSmax y d2Smax:",maxDS_aprox-maxD2S_aprox)

ax.legend(loc='upper left')

fig.tight_layout()
fig.savefig("p_18b", dpi=200)


#-----------------------------------------------------------------------------------------
#PROBLEMA 19
#hacerlo para varios Dmax!!!

L=1000
Dmax=17
m_max=int(np.log2(L)) #m_max es hasta donde voy con los m
m_arr=np.zeros(m_max-3)

fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(111)
for i in range(3,m_max):
    dims_arr=np.linspace(0,Dmax,N)
    m=i+1 #defino m, en este caso 4
    S_aprox = [(L/(2**(m-1)))**Dmax * Shannon(2**(d-Dmax), log=2) * (2**(m*d)-1) / (2**d-1) / (L**Dmax)  for d in dims_arr] #calculo entropia normalizada con L**Dmax
    S_aprox[0] = (L/(2**(m-1)))**Dmax * Shannon(2**(-Dmax), log=2) * m /(L**Dmax)  #que los arreglo con esto
    m_arr[i-3] = np.amax(S_aprox) #saco maximo
    ax.plot(dims_arr,S_aprox,label="m = "+str(m))
    ax.plot(dims_arr, 0.5*np.ones(len(dims_arr)), color = "k")

ax.legend()
ax.set_xlabel("D")
ax.set_ylabel("S")
fig.tight_layout()
fig.savefig("p_19_"+str(Dmax), dpi=200)
quit()



#-----------------------------------------------------------------------------------------
#PROBLEMA 20

L = 100
m = int(np.log2(L/2.))

D=50
dims_arr=np.linspace(46,50,1000) 
S_aprox=[(L/2**(m-1))**D * Shannon(2**(d-D), log = 2) * (2**(m*d)-1) / (2**d-1) for d in dims_arr] #la entropía aproximada
S_aprox[0]=(L/2**(m-1))**D * Shannon(2**(-D), log = 2) * m #tiene un error en cero, asi que ahí la defino como el límite

maxS=dims_arr[np.argmax(S_aprox)] #d1
maxDS=dims_arr[np.argmax(np.gradient(S_aprox))] #d2


DS=np.gradient(S_aprox)/(dims_arr[1]-dims_arr[0]) #derivada
ERR=np.zeros((100,100))
b_arr = []
A_arr = []

for i in range(100):
    print(i)
    b=i/100.
    for j in range(100):
        A=j/50.
        #muevo y reescalo S:
        ST=np.array([A*(L/2**(m-1))**D * Shannon(2**(d+b-D), log=2) * (2**(m*(d+b))-1) / (2**(d+b)-1) for d in dims_arr])
        #guardo el error cuadrático medio
        Serr=np.sum((np.array(DS[np.where(dims_arr<50-b)])-np.array(ST[np.where(dims_arr<50-b)]))**2) 
        ERR[i,j]=Serr
        b_arr.append(b)
        A_arr.append(A)

#minimizo el rmse
mins = np.argmin(ERR)
print("b analítico:", maxS-maxDS,"b encontrado: ",b_arr[mins], "A encontrado",A_arr[mins])


b=b_arr[mins] #traslacion
A=A_arr[mins] #escaleo

ST=np.array([A*(L/2**(m-1))**D * Shannon(2**(d+b-D), log=2) * (2**(m*(d+b))-1) / (2**(d+b)-1) for d in dims_arr]) #S movida y escaleada
Serr=np.sum((np.array(DS[np.where(dims_arr<50-b)])-np.array(ST[np.where(dims_arr<50-b)]))**2) #error a minimizar


fig = plt.figure(figsize=(6,4))
plt.plot(dims_arr,S_aprox,label=r'$S_{aprox}(D)$')
plt.plot(dims_arr,DS,label=r'$dS_{aprox}(D)/dD$')
plt.plot(dims_arr,ST,label=r'$1.46 \, S_{aprox}(D+ 0.23)$')
plt.ylim([0,6e99])
plt.xlim([48,50.1])
plt.legend()
plt.xlabel("D")
fig.tight_layout()
fig.savefig("p_20", dpi=200)
