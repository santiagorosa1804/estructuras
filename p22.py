import numpy as np
import numpy.random as rnd
from scipy.stats import linregress
from itertools import product
from skimage.util.shape import view_as_blocks
import matplotlib.pyplot as plt
import matplotlib.colors as col
import math
import random
#colors = ["#222233","#F7FF00","#0099CC"]
#cmap1 = col.LinearSegmentedColormap.from_list("mycmap", colors)


#-----------------------------------------------------------------------------------------------------------------------

def GetNumbers(A,D,tol,N): #crea el array Nk
    if ((N & (N-1) == 0) and N != 0):
        s=int(math.log2(N//2))
        if (2**(s*D)<A) and (N*N*2**(s*(D-2))>A):
            Check=True
            dd=0
            extratol=0
            while Check:
                Nu=[int(np.random.poisson(lam=A*2**(-i*D))) for i in range(s+1)]
                Nu[0]=A
                dd+=1
                if (Nu[s]>1) and (Nu[s]<=4):
                    Check=False
                    NNU=[-math.log2(Nu[i]+(Nu[i]==0)) for i in range(len(Nu))]
                    Stats=linregress(np.linspace(0,s,s+1), NNU)
                    if abs(D-Stats[0])>tol+extratol:
                        Check=True
                else:
                    Check=True
                for i in range(s):
                    if (Nu[i]<Nu[i+1]) or (Nu[i]>=4*Nu[i+1]):
                        Check=True
                    elif (dd>1000):
                        extratol=extratol+0.01
                        dd=0
            return Nu,Stats[0]

        else:
            raise Exception("No se puede encontrar condición inicial para esa Área y Dimensión")
    else:
        raise Exception("El tamaño debe ser potencia de 2")

def get_nboxes_list(r,l,D,D_epsilon = 0.01):

    '''
    obtain random Nk list.

    Input: 
        r: percent area coefficient.
        l: grid length.
        D: dimension.
        D_epsilon: dimension uncertainty.

    Output:
        Nk list.  
    '''

    npixels = r*l**2
    nit = int(np.log2(l))

    Nk0 = [round(npixels*2**(-k*D)) for k in range(nit)]   #number of cells with pixels in the k-th iteration
    k_lst = [2**(k+1) for k in range(nit)] #boxes size
    k_lst_inv = [k**-1 for k in k_lst]

    cond = False
    dim_change = False
    j=0
    while cond == False:
        Nk0_new = [round(rnd.poisson(npixels * 2**(-k*D))) for k in range(nit)]
        #Nk0_new = [round(Nk + np.random.normal(loc = 0, scale = 0.1*Nk)) for Nk in Nk0 ]
        Nk0_new[0] = Nk0[0]
        result = linregress(np.log(k_lst_inv), np.log(Nk0_new))
        a = result[0]; Ua = result[4]

        check = True
        for i in range(1,len(Nk0_new)):
            if 4*Nk0_new[i] < Nk0_new[i-1]:
                check = False

        if abs(a-D)<D_epsilon and check is True and Nk0_new[-1]<5:
            cond = True

        j+=1
        if j%5000==0: 
            D_epsilon+=0.01
            dim_change = True

    Nk = [0]
    for item in Reverse(Nk0_new): Nk.append(item)
    Nk0 = Reverse(Nk)
    #print("dim=",a)

    return Reverse(Nk0), dim_change


#-------------------------------

#-----------------------------------------------------------------------------------------------------------------------
        
def MinMax(D,N): #Me da Amin y Amax dada una dimensión D
    s=int(math.log2(N//2))
    #print([2**(s*D),N*N*2**(s*(D-2))])
    return [2**(s*D),N*N*2**(s*(D-2))]


#-----------------------------------------------------------------------------------------------------------------------

def aMinMax(A,N): #Me da Dmin y Dmax dada un área A
    s=int(math.log2(N//2))
    #print([2.0+math.log2(A/N/N)/s,math.log2(A)/s])
    return [2.0+math.log2(A/N/N)/s,math.log2(A)/s]


#-----------------------------------------------------------------------------------------------------------------------
 
def Ocuppation(Ma,k,N): #Devuelve N(k) de la matriz Ma
    Lar=N//k
    Cant=N//(2**k)
    B = view_as_blocks(Ma, block_shape=(Cant, Cant))
    Ocup = [np.unique(B[i][j]) for i,j in product(range(len(B)), range(len(B)))]
    O = 0
    for i in range(len(Ocup)):
        if (not np.all(Ocup[i])):
            O=O+1
    return O

#-----------------------------------------------------------------------------------------------------------------------

def Count2(Ma):
    Celestes=0
    Amarillos=0
    Apaticos=0
    for i in range(len(Ma)):
        for j in range(len(Ma)):
            if Ma[i,j]!=0:
                if Ma[i,j]==1:
                    Amarillos+=1
                if Ma[i,j]==2:
                    Celestes+=1
            else:
                Apaticos+=1
    print("Cel=",Celestes,"Ama=",Amarillos,"Apatics=",Apaticos,"Counted=",Celestes+Amarillos+Apaticos,"L*L=",len(Ma)*len(Ma))

#-----------------------------------------------------------------------------------------------------------------------

def get_inv_dict(dict0):
    '''
    invert choices dictionary i-> (x,y) yo (x,y) -> i        
    '''
    inv_dict = {}
    for item in dict0:
        inv_dict[(dict0[item][0],dict0[item][1])] = item

    return inv_dict
def Reverse(lst):
    new_lst = lst[::-1]
    return new_lst

#-----------------------------------------------------------------------------------------------------------------------

def get_two_structured(AreaA,AreaC,L,NumbersA,NumbersB):
    Tries=1
    Failed=True
    nit = int(np.log2(L))
    ratio = int(L/2)
    while Failed:
        Failed=False
        # initialize grid where i can throw pixels into. Starts as a 2x2 matrix
        target_gridA = np.ones((2,2))
        target_gridB = np.ones((2,2))
        # make target grid as a dictionary, for convenience
        target_grid_dictA = {}
        target_grid_listA = []
        target_grid_dictB = {}
        target_grid_listB = []
        idx = 0
        li = 2       # size of the target grid

        #create the first 2x2 lists to choose from
        for i in range (li):
            for j in range (li):
                target_grid_dictA[idx] = [i*ratio,j*ratio]
                target_grid_listA.append(idx)
                target_grid_dictB[idx] = [i*ratio,j*ratio]
                target_grid_listB.append(idx)
                idx+=1

        for it in range(1,len(NumbersA)):

            if it==nit: break

            NpixelsA = NumbersA[it] - NumbersA[it-1]     # new pixels to generate in this iteration
            NpixelsB = NumbersB[it] - NumbersB[it-1]     # new pixels to generate in this iteration
            
            # select areas where pixels will be thrown
            area_choicesA = rnd.choice(target_grid_listA,NumbersA[it], replace = False)
            area_choicesB = rnd.choice(target_grid_listB,NumbersB[it], replace = False)
           
            # remove pixelless sections
            for item in target_grid_listA:
                if item not in area_choicesA:
                    target_grid_dictA.pop(item)
            for item in target_grid_listB:
                if item not in area_choicesB:
                    target_grid_dictB.pop(item)

            #save old dicts
            dict0A = target_grid_dictA
            dict0B = target_grid_dictB

            # update target grid
            dict1A = {}  
            dict1B = {} 
            countA = 0
            countB = 0
            new_ratio = int(ratio/2)    # uptade ratio
            li *= 2       # update target area box size 

            for ii in range (li):
                for jj in range(li):
                    for item in target_grid_dictA:
                        if ii*new_ratio >= target_grid_dictA[item][0] and ii*new_ratio< target_grid_dictA[item][0] + ratio and jj*new_ratio >= target_grid_dictA[item][1] and jj*new_ratio< target_grid_dictA[item][1] + ratio:
                            dict1A.update({countA: [ii*new_ratio,jj*new_ratio]})
                            countA += 1
                    for item in target_grid_dictB:
                        if ii*new_ratio >= target_grid_dictB[item][0] and ii*new_ratio< target_grid_dictB[item][0] + ratio and jj*new_ratio >= target_grid_dictB[item][1] and jj*new_ratio< target_grid_dictB[item][1] + ratio:
                            dict1B.update({countB: [ii*new_ratio,jj*new_ratio]})
                            countB += 1

            target_grid_listA = [i for i in range(countA)]   # actual list to choose from
            target_grid_dictA = dict1A
            target_grid_listB = [i for i in range(countB)]   # actual list to choose from
            target_grid_dictB = dict1B
            ratio = new_ratio       # update ratio

        # get inverse grid dicts. Will be useful later    
        inv_target_grid_dictA = get_inv_dict(target_grid_dictA)
        inv_target_grid_dictB = get_inv_dict(target_grid_dictB)
        grid = np.zeros((L,L),dtype=int)  #initialize grid

        # place one pixel in each box of the Nth-1 boxes    
        for item in dict0A:
            dum_dict = {}
            dum_list = []
            count = 0
            for i in range (2):
                for j in range(2):
                    dum_dict[count] = [dict0A[item][0]+i , dict0A[item][1]+j]
                    dum_list.append(count)
                    count+=1
            pixel = rnd.choice(dum_list)

            grid[dum_dict[pixel][0] , dum_dict[pixel][1]] = 1   # place pixel

            # use inv dict to know wich pixel hast been selected already
            sel_px = inv_target_grid_dictA[dum_dict[pixel][0], dum_dict[pixel][1]]
            target_grid_listA.remove(sel_px) #remove from pool choices
            try:
                sel_pxx = inv_target_grid_dictB[dum_dict[pixel][0], dum_dict[pixel][1]]
                target_grid_listB.remove(sel_pxx) # also remove from the other list if it's there
            except KeyError:
                pass

        for item in dict0B:
            dum_dict = {}
            dum_list = []
            count = 0
            for i in range (2):
                for j in range(2):
                    dum_dict[count] = [dict0B[item][0]+i , dict0B[item][1]+j]
                    dum_list.append(count)
                    count+=1
            pixel = rnd.choice(dum_list)

            # this is new, it prevents from overwriting the other pattern
            while(grid[dum_dict[pixel][0] , dum_dict[pixel][1]] == 1): 
                pixel = rnd.choice(dum_list)

            grid[dum_dict[pixel][0] , dum_dict[pixel][1]] = 2   # place pixel

            # use inv dict to know wich pixel hast been selected already
            sel_px = inv_target_grid_dictB[dum_dict[pixel][0], dum_dict[pixel][1]]
            target_grid_listB.remove(sel_px) # remove from pool choices
            try:
                sel_pxx = inv_target_grid_dictA[dum_dict[pixel][0], dum_dict[pixel][1]]
                target_grid_listA.remove(sel_pxx) # also remove from the other list if it's there
            except KeyError:
                pass

        # place the rest of the pixels, first, as usual...
        try:
            pixel_choices = rnd.choice(target_grid_listA,NumbersA[-1] - NumbersA[-2], replace = False)
        except ValueError:
            Tries=Tries+1
            Failed=True
        for pixel in pixel_choices:
            grid[target_grid_dictA[pixel][0],target_grid_dictA[pixel][1]] = 1

        # then we need to brute force it...
        TryManyB=NumbersB[-1] - NumbersB[-2] # how many left
        while(TryManyB>0):
            trying=0
            pixel=rnd.choice(target_grid_listB, replace = False)
            while(grid[target_grid_dictB[pixel][0] , target_grid_dictB[pixel][1]] != 0): 
                pixel = rnd.choice(target_grid_listB, replace = False)
                trying+=1
                '''If the following condition meets, one should redoo the whole process from scratch!'''
                if (trying>100*L):
                    Tries=Tries+1
                    Failed=True
                    break
            grid[target_grid_dictB[pixel][0],target_grid_dictB[pixel][1]] = 2
            TryManyB=TryManyB-1
            
    return Tries,grid



#-----------------------------------------------------------------------------------------------------------------------
l=64
r1 = 0.1
r2 = 0.15

Dmin1, Dmax1 = aMinMax(r1*l**2,l)
Dmin2, Dmax2 = aMinMax(r2*l**2,l)

print(Dmin1,Dmin2)

p =0.

NkA,_ = get_nboxes_list(r1,l,Dmin1,D_epsilon = 0.02) #Nk amarillo
NkB,_ = get_nboxes_list(r2,l,Dmin2,D_epsilon = 0.02) #Nk celeste

print(NkA,NkB)

Tries,grid=get_two_structured(r1*l**2,r2*l**2,l,NkA,NkB)
print("Intentos=",Tries)

import os

exp_folder = "tmp/p22"
try: os.mkdir(exp_folder)
except: pass

import seaborn as sns
fig = plt.figure(figsize=(8,6))
ax = sns.heatmap( grid ,cmap = 'coolwarm' )
ax.set(xticklabels=[])
ax.set(yticklabels=[])
fig.savefig(exp_folder+"/grid.png", dpi=200)
