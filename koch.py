import numpy as np
import matplotlib.pyplot as plt


def Distance(x1,x2):
    '''
    Funcion para calcular la distancia entre dos puntos en 2D.
    '''
    
    return np.sqrt( (x1[0]-x2[0])**2 + (x1[1]-x2[1])**2 )
    

def Rotation2D(vertices,angle):
    '''
    Funcion para rotar un conjunto de puntos en 2D.

    Input: 
        vertices (numpy array): array de dim (nvert,2),
        donde nvert es el numero de puntos a rotar.

        angle (float): angulo de rotacion (en radianes) 
    
    Output: vertices_rotated (numpy array):
            conjunto de puntos rotados.
    '''
    
    nvert = vertices.shape[0]
    vertices_rotated = np.copy(vertices)
    for i in range (nvert):
        
        y = np.zeros(2)
        cos = np.cos(angle)
        sin = np.sin(angle)

        y[0] = vertices[i,0]*cos - vertices[i,1]*sin
        y[1] = vertices[i,0]*sin + vertices[i,1]*cos
        
        vertices_rotated[i] = y
        
    return vertices_rotated

#-----------------------------------------------------------------------------
def Traslation2D(vertices,a):
    '''
    Funcion para trasladar un conjunto de puntos en 2D.

    Input: 
        vertices (numpy array): array de dim (nvert,2),
        donde nvert es el numero de puntos a trasladar.

        a (numpy array): vector de traslacion. 
    
    Output: vertices_rotated (numpy array):
            conjunto de puntos trasladados.
    '''

    nvert = vertices.shape[0]
    vertices_translated = np.copy(vertices)
    
    for i in range(nvert):
        
        y = np.zeros(2)

        y[0] = vertices[i,0] + a[0]
        y[1] = vertices[i,1] + a[1]
        
        vertices_translated[i] = y
        
    return vertices_translated
#-----------------------------------------------------------------------------

def Koch(Nit=0, npoints = 10):

    '''
    Funcion para calcular los putos de la curva de Koch.
    Input: 
        Nit (Int): iteraciones de la curva de Koch. Nit=0 (default)
        corresponde a la primer iteracion (un solo triangulo).

        npoints (Int): numero de puntos intermedios entre vertices de 
        la curva de Koch.    

    '''

    #make the vertices of the Koch curve.
    #Nit: number of iterations

    pi = np.arccos(-1.)

    #vertices of the first iteration of the Koch curve
    vertices = np.zeros((5,2))
    vertices[1,0] = 1./3.
    vertices[2,0] = 0.5
    vertices[2,1] = 1./6. * np.sqrt(3)
    vertices[3,0] = 2./3.
    vertices[4,0] = 1.

    nver = 5   #number of vertices of the curve. will be updated in each iteration

    for i in range (Nit):
        print(i)    
        vertices0 = np.zeros((4*nver-3,2))  #array with the new iteration vertices
        
        vert0 = 1./3. * np.copy(vertices)
        vertices0[0:nver] = vert0       #append first part
        
        vert0 = 1./3. * np.copy(vertices)
        vert0 = Rotation2D(vert0, pi/3.) #apply rotation
        vert0 = Traslation2D(vert0,[1./3.,0.]) #apply traslation
        vertices0[nver:2*nver-1] = vert0[1:]       #append first part
        
        vert0 = 1./3. * np.copy(vertices)
        vert0 = Rotation2D(vert0, -pi/3.) #apply rotation
        vert0 = Traslation2D(vert0,[1./2.,np.sqrt(3)/6.]) #apply traslation
        vertices0[2*nver-1:3*nver-2] = vert0[1:]       #append first part
        
        vert0 = 1./3. * np.copy(vertices)
        vertices0[0:nver] = vert0       #append first part
        vert0 = Traslation2D(vert0,[2./3.,0.]) #apply traslation
        vertices0[3*nver-2:] = vert0[1:]       #append first part
        
        nver = 4*nver-3         #update nver
        vertices = vertices0    

    if npoints is not None:

        #add npoints-2 between each vertices defining an edge in the koch curve
        vertices_full = np.zeros(( nver+(nver-1)*(npoints-1),2))
        for i in range(vertices.shape[0]-1):
            vertices_full[i*npoints:(i+1)*npoints,0] = np.linspace(vertices[i,0],vertices[i+1,0], num=npoints)
            vertices_full[i*npoints:(i+1)*npoints,1] = np.linspace(vertices[i,1],vertices[i+1,1], num=npoints)


        return vertices_full
    else: return vertices    
#-----------------------------------------------------------------------------

def Correlation(Set, Nit=3):
    
    '''
    Funcion para calcular la dimension de correlacion de
    un conjunto bidimensional. la longitud de la grilla 
    es 4**-k, adaptado al problema de la curva de Koch.

    Input:
        Set (numpy array): puntos del conjunto.
        Nit (Int): exponente maximo  de la longitud
        de las cajas para calcular la dimension.
    Output:
        dim_list, length_list (listas): listas con las
        dimensiones y longitud de cajas. 

    '''

    dim_list = []
    length_list = []

    for koch_it in range(Nit):
        #correlation measure
        n_grid_points = 4**(koch_it+1)
        delta = 1./n_grid_points

        ntot = Set.shape[0]   #number of points in the curve
        #compute correlation
        nn=0  #initialize count 
        for i in range (ntot):
             for j in range (ntot):
                if Distance( Set[i,:], Set[j,:] ) < delta:
                    nn+=1
        corr = nn/(ntot*(ntot-1))

        dim_list.append( np.log(corr) / np.log(delta) )
        length_list.append(delta)        

    return dim_list, length_list

#-----------------------------------------------------------------------------

def Information(Set,Nit=3):
    '''
    Funcion para calcular la dimension de informacion de
    un conjunto bidimensional. la longitud de la grilla 
    es 4**-k, adaptado al problema de la curva de Koch.

    Input:
        Set (numpy array): puntos del conjunto.
        Nit (Int): exponente maximo  de la longitud
        de las cajas para calcular la dimension.
    Output:
        dim_list, length_list (listas): listas con las
        dimensiones y longitud de cajas. 

    '''

    npoints = Set.shape[0]
    dim_list = []
    length_list = []

    for koch_it in range(Nit):
        print(koch_it)
        #correlation measure
        n_grid_points = 2**(koch_it+1)
        min_x = np.amin(Set[:,0])        
        max_x = np.amax(Set[:,0])    
        min_y = np.amin(Set[:,1])        
        max_y = np.amax(Set[:,1])    

        delta = 1./n_grid_points*(max_x-min_x)

        ntot = Set.shape[0]   #number of points in the curve
        
        #compute probabilities
        prob_list = []
        for i in range(n_grid_points):
            for j in range(n_grid_points):
                count = 0
                for idx in range(npoints):
                    if Set[idx,0]>min_x+delta*i and Set[idx,0]<min_x+delta*(i+1) and Set[idx,1]>min_y+delta*j and Set[idx,1]<min_y+delta*(j+1):
                        count+=1
                if count !=0: prob_list.append(count/npoints)     #save probability of a point being in the j-th box
#                prob_list_updated = [item for item in prob_list if item != 0.]   #remove p=0

        s = -np.sum( prob_list * np.log(prob_list) )    #Shannon entropy
        dim = -s/np.log(delta)

        dim_list.append(dim)
        length_list.append(delta)

    return  dim_list, length_list

    #-----------------------------------------------------------------------------

def Box_counting(Set,Nit=3):
    '''
    Funcion para calcular la dimension de informacion de
    un conjunto bidimensional. la longitud de la grilla 
    es 2**-k, adaptado al problema de la curva de Koch.

    Input:
        Set (numpy array): puntos del conjunto.
        Nit (Int): exponente maximo  de la longitud
        de las cajas para calcular la dimension.
    Output:
        dim_list, length_list (listas): listas con las
        dimensiones y longitud de cajas. 

    '''

    npoints = Set.shape[0]
    dim_list = []
    length_list = []

    for it in range(Nit):
        print(it)
        #correlation measure
        n_grid_points = 2**(it+1)
        
        min_x = np.amin(Set[:,0])        
        max_x = np.amax(Set[:,0])    
        min_y = np.amin(Set[:,1])        
        max_y = np.amax(Set[:,1])    

        delta = 1./n_grid_points*(max_x-min_x)

        ntot = Set.shape[0]   #number of points in the curve
        
        #count boxes with points
        box_count = 0
        for i in range(n_grid_points):
            for j in range(int(n_grid_points/2)):
                for idx in range(npoints):
                    if Set[idx,0]>min_x+delta*i and Set[idx,0]<min_x+delta*(i+1) and Set[idx,1]>min_y+delta*j and Set[idx,1]<min_y+delta*(j+1):
                        box_count+=1
                        break

        dim = -np.log(box_count)/np.log(delta)

        dim_list.append(dim)
        length_list.append(delta)

    return  dim_list, length_list