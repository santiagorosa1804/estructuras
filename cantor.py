import numpy as np
import numpy.random as rnd

def remove_center(Inter):
    '''
    Funcion para remover el centro (entre 1/3 y 2/3) 
    de un intervalo de puntos. 
    Input: 
        Inter (lista): intervalo.
    Output:
        Inter1 (lista), Inter2 (lista): listas con los puntos 
        correspondientes a las fracciones [0,1/3] y [2/3,1] de Inter.
    '''
    size = Inter[-1] - Inter[0]
    
    condition1 = np.where((Inter > Inter[0] + size/3.))
    condition2 = np.where((Inter < Inter[0] + size*2./3. ))
    
    Inter1 = list(np.delete(Inter,condition1))
    Inter2 = list(np.delete(Inter,condition2))
    
    return Inter1,Inter2

#-----------------------------------------------------------------------

def Cantor(Nit, Npoints=5000):
    '''
    Funcion para crear un conjunto de Cantor. 
    Input: 
        Nit (Int): iteraciones del conjunto de Cantor.
        Npoints (Int): numero de puntos con que empiezo las iteraciones. 
            Voy a remover 1/3 en cada iteracion. 
    Output:
        cantor_array (numpy array 1D): puntos del conjunto de Cantor.
    '''

    interval = np.linspace(0., 1., num=Npoints, dtype='float')
    interval = [list(interval)]
        
    for i in range(Nit):

        interval0 = []    
        for Inter in (interval):
            
            if len(Inter) != 0:
                Inter1, Inter2 = remove_center(Inter)
            
                interval0.append(Inter1)
                interval0.append(Inter2)
            
        interval = interval0
        
    cantor_array = np.empty(0)
    for item in interval:
        cantor_array = np.concatenate((cantor_array,np.array(item)))    
    
    return cantor_array

#-----------------------------------------------------------------------

def BoxCounting1D(Set,k):
    '''
    Funcion para calcular la dimension de 
    box-counting de un conjunto 1D.
    Input: 
        Set (array 1D): conjunto de puntos 
        al que quiero calcularle la dimension.
        k (Int): exponente del largo de la grilla: 2**-k. 
    Output:
        dim (float): dimension aproximada de box-counting.
    '''
    
    min0 = np.min(Set)
    max0 = np.max(Set)

    delta = (max0-min0)*2**-k       #box size
    nboxes = 2**k       #number of boxes
    print(nboxes)
    print(min0,max0)
    N = 0
    for j in range(nboxes):
        for item in Set:
            if item>min0+delta*j and item < min0+delta*(j+1):
                N+=1
                break

    return -np.log(N)/np.log(2**-k)
