import numpy as np
import matplotlib.pyplot as plt
import patron2d
import os
import seaborn as sns

exp_folder = "tmp/p24"
try: os.mkdir(exp_folder)
except: pass


nreps = 10

l = 64
D = 1.5
D_epsilon = 0.03
m = np.log2(l/2.)
r_arr = np.linspace(0.05,0.4,10)

dim_ini_lst = []
dim_fin_lst = []

for r in r_arr:
    print(r)
    dim_ini = 0
    dim_fin = 0
    for rep in range(nreps):
        print(r)
        #grid, _ = patron2d.Pattern2D(r,l,D,D_epsilon,graph_name=exp_folder+"/patron_inicial", null_px_ratio = 0.01)
        grid, _ = patron2d.Pattern2D(.15,l,1.4,D_epsilon, null_px_ratio = 0.01)
        # grid, _ = patron2d.Pattern2D(r,l,D,D_epsilon, null_px_ratio = 0.01)


        #fig = plt.figure(figsize=(8,6))
        #ax = sns.heatmap( grid ,cmap = 'coolwarm' )
        #ax.set(xticklabels=[])
        #ax.set(yticklabels=[])
        #fig.savefig("tmp/p24/grid_antes.png", dpi=200)


        Sch = patron2d.Scheling(grid)
        
        dim_ini += Sch.ComputeDim(grid)


        border = Sch.get_border(grid)
        Sch.get_unsatisfied(grid)

        condition = True

        # while condition == True:
        for i in range(2000):
            grid = Sch.step(grid)
            unsatisfied = Sch.get_unsatisfied(grid)

        dim_fin += Sch.ComputeDim(grid)


        #fig = plt.figure(figsize=(8,6))
        #ax = sns.heatmap( grid ,cmap = 'coolwarm' )
        #ax.set(xticklabels=[])
        #ax.set(yticklabels=[])
        #fig.savefig("tmp/p24/grid_despues.png", dpi=200)

        #quit()

    dim_ini_lst.append(dim_ini/nreps)
    dim_fin_lst.append(dim_fin/nreps)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(r_arr,dim_ini_lst, label = "ini")
ax.plot(r_arr,dim_fin_lst, label = "fin")
ax.legend()
fig.savefig("tmp/p24.png",dpi=300)
plt.show()
