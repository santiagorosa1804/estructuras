import numpy as np
import numpy.random as rnd
from scipy.stats import linregress
from itertools import product
import matplotlib.pyplot as plt
import time
import seaborn as sns
#-------------------------------
def Reverse(lst):
    new_lst = lst[::-1]
    return new_lst
#-------------------------------
def Count_points(grid):
    l = grid.shape[0]
    count = 0
    for l1 in range(l):
        for l2 in range(l):
            if grid[l1,l2]==1:
                count+=1

    return count
#-------------------------------
def get_nboxes_list(r,l,D,D_epsilon = 0.01):

    '''
    obtain random Nk list.

    Input: 
        r: percent area coefficient.
        l: grid length.
        D: dimension.
        D_epsilon: dimension uncertainty.

    Output:
        Nk list.  
    '''

    npixels = r*l**2
    nit = int(np.log2(l))

    Nk0 = [round(npixels*2**(-k*D)) for k in range(nit)]   #number of cells with pixels in the k-th iteration
    k_lst = [2**(k+1) for k in range(nit)] #boxes size
    k_lst_inv = [k**-1 for k in k_lst]

    cond = False
    dim_change = False
    j=0
    while cond == False:
        Nk0_new = [round(rnd.poisson(npixels * 2**(-k*D))) for k in range(nit)]
        #Nk0_new = [round(Nk + np.random.normal(loc = 0, scale = 0.1*Nk)) for Nk in Nk0 ]
        Nk0_new[0] = Nk0[0]
        result = linregress(np.log(k_lst_inv), np.log(Nk0_new))
        a = result[0]; Ua = result[4]

        check = True
        for i in range(1,len(Nk0_new)):
            if 4*Nk0_new[i] < Nk0_new[i-1]:
                check = False

        if abs(a-D)<D_epsilon and check is True and Nk0_new[-1]<5:
            cond = True

        j+=1
        if j%5000==0: 
            D_epsilon+=0.01
            dim_change = True

    Nk = [0]
    for item in Reverse(Nk0_new): Nk.append(item)
    Nk0 = Reverse(Nk)
    #print("dim=",a)

    return Reverse(Nk0), dim_change


#-------------------------------
def get_inv_dict(dict0):

    '''
    invert choices dictionary i-> (x,y) yo (x,y) -> i        
    '''

    inv_dict = {}
    for item in dict0:
        inv_dict[(dict0[item][0],dict0[item][1])] = item

    return inv_dict

#-------------------------------

def get_two_structured(AreaA,AreaC,L,NumbersA,NumbersB):
    Tries=1
    Failed=True
    nit = int(np.log2(L))
    ratio = int(L/2)
    while Failed:
        Failed=False
        # initialize grid where i can throw pixels into. Starts as a 2x2 matrix
        target_gridA = np.ones((2,2))
        target_gridB = np.ones((2,2))
        # make target grid as a dictionary, for convenience
        target_grid_dictA = {}
        target_grid_listA = []
        target_grid_dictB = {}
        target_grid_listB = []
        idx = 0
        li = 2       # size of the target grid

        #create the first 2x2 lists to choose from
        for i in range (li):
            for j in range (li):
                target_grid_dictA[idx] = [i*ratio,j*ratio]
                target_grid_listA.append(idx)
                target_grid_dictB[idx] = [i*ratio,j*ratio]
                target_grid_listB.append(idx)
                idx+=1

        for it in range(1,len(NumbersA)):

            if it==nit: break

            NpixelsA = NumbersA[it] - NumbersA[it-1]     # new pixels to generate in this iteration
            NpixelsB = NumbersB[it] - NumbersB[it-1]     # new pixels to generate in this iteration
            
            # select areas where pixels will be thrown
            area_choicesA = rnd.choice(target_grid_listA,NumbersA[it], replace = False)
            area_choicesB = rnd.choice(target_grid_listB,NumbersB[it], replace = False)
           
            # remove pixelless sections
            for item in target_grid_listA:
                if item not in area_choicesA:
                    target_grid_dictA.pop(item)
            for item in target_grid_listB:
                if item not in area_choicesB:
                    target_grid_dictB.pop(item)

            #save old dicts
            dict0A = target_grid_dictA
            dict0B = target_grid_dictB

            # update target grid
            dict1A = {}  
            dict1B = {} 
            countA = 0
            countB = 0
            new_ratio = int(ratio/2)    # uptade ratio
            li *= 2       # update target area box size 

            for ii in range (li):
                for jj in range(li):
                    for item in target_grid_dictA:
                        if ii*new_ratio >= target_grid_dictA[item][0] and ii*new_ratio< target_grid_dictA[item][0] + ratio and jj*new_ratio >= target_grid_dictA[item][1] and jj*new_ratio< target_grid_dictA[item][1] + ratio:
                            dict1A.update({countA: [ii*new_ratio,jj*new_ratio]})
                            countA += 1
                    for item in target_grid_dictB:
                        if ii*new_ratio >= target_grid_dictB[item][0] and ii*new_ratio< target_grid_dictB[item][0] + ratio and jj*new_ratio >= target_grid_dictB[item][1] and jj*new_ratio< target_grid_dictB[item][1] + ratio:
                            dict1B.update({countB: [ii*new_ratio,jj*new_ratio]})
                            countB += 1

            target_grid_listA = [i for i in range(countA)]   # actual list to choose from
            target_grid_dictA = dict1A
            target_grid_listB = [i for i in range(countB)]   # actual list to choose from
            target_grid_dictB = dict1B
            ratio = new_ratio       # update ratio

        # get inverse grid dicts. Will be useful later    
        inv_target_grid_dictA = get_inv_dict(target_grid_dictA)
        inv_target_grid_dictB = get_inv_dict(target_grid_dictB)
        grid = np.zeros((L,L),dtype=int)  #initialize grid

        # place one pixel in each box of the Nth-1 boxes    
        for item in dict0A:
            dum_dict = {}
            dum_list = []
            count = 0
            for i in range (2):
                for j in range(2):
                    dum_dict[count] = [dict0A[item][0]+i , dict0A[item][1]+j]
                    dum_list.append(count)
                    count+=1
            pixel = rnd.choice(dum_list)

            grid[dum_dict[pixel][0] , dum_dict[pixel][1]] = 1   # place pixel

            # use inv dict to know wich pixel hast been selected already
            sel_px = inv_target_grid_dictA[dum_dict[pixel][0], dum_dict[pixel][1]]
            target_grid_listA.remove(sel_px) #remove from pool choices
            try:
                sel_pxx = inv_target_grid_dictB[dum_dict[pixel][0], dum_dict[pixel][1]]
                target_grid_listB.remove(sel_pxx) # also remove from the other list if it's there
            except KeyError:
                pass

        for item in dict0B:
            dum_dict = {}
            dum_list = []
            count = 0
            for i in range (2):
                for j in range(2):
                    dum_dict[count] = [dict0B[item][0]+i , dict0B[item][1]+j]
                    dum_list.append(count)
                    count+=1
            pixel = rnd.choice(dum_list)

            # this is new, it prevents from overwriting the other pattern
            while(grid[dum_dict[pixel][0] , dum_dict[pixel][1]] == 1): 
                pixel = rnd.choice(dum_list)

            grid[dum_dict[pixel][0] , dum_dict[pixel][1]] = 2   # place pixel

            # use inv dict to know wich pixel hast been selected already
            sel_px = inv_target_grid_dictB[dum_dict[pixel][0], dum_dict[pixel][1]]
            target_grid_listB.remove(sel_px) # remove from pool choices
            try:
                sel_pxx = inv_target_grid_dictA[dum_dict[pixel][0], dum_dict[pixel][1]]
                target_grid_listA.remove(sel_pxx) # also remove from the other list if it's there
            except KeyError:
                pass

        # place the rest of the pixels, first, as usual...
        try:
            pixel_choices = rnd.choice(target_grid_listA,NumbersA[-1] - NumbersA[-2], replace = False)
        except ValueError:
            Tries=Tries+1
            Failed=True
        for pixel in pixel_choices:
            grid[target_grid_dictA[pixel][0],target_grid_dictA[pixel][1]] = 1

        # then we need to brute force it...
        TryManyB=NumbersB[-1] - NumbersB[-2] # how many left
        while(TryManyB>0):
            trying=0
            pixel=rnd.choice(target_grid_listB, replace = False)
            while(grid[target_grid_dictB[pixel][0] , target_grid_dictB[pixel][1]] != 0): 
                pixel = rnd.choice(target_grid_listB, replace = False)
                trying+=1
                '''If the following condition meets, one should redoo the whole process from scratch!'''
                if (trying>100*L):
                    Tries=Tries+1
                    Failed=True
                    break
            grid[target_grid_dictB[pixel][0],target_grid_dictB[pixel][1]] = 2
            TryManyB=TryManyB-1
            
    return grid, Tries

#-------------------------------

def Pattern2D(r,l,D,D_epsilon, graph_name=None, null_px_ratio = None):

    '''
    Create 2d pattern.

    Input:
        r: percent area coefficient.
        l: grid length.
        D: dimension.
        D_epsilon: dimension uncertainty.    
        graph: default plot.
        null_px_ratio: percent of empty pixels.
    
    Output:
        grid: 2d numpy array with the pattern. 
    '''

    max_res = 1./l
    npixels = r*l**2
    nit = int(np.log2(l))
    ratio = int(l/2) #ratio final grid size / iteration grid size

    if null_px_ratio is not None:
        null_pixels = round(null_px_ratio*l**2)

    Nk0, dim_change = get_nboxes_list(r,l,D,D_epsilon)   #number of cells with pixels in the k-th iteration
    print(dim_change,"cuadrados a llenar en cada iteracion:",Nk0)

    #initialize grid where i can throw pixels into. Starts as a 2x2 matrix
    target_grid = np.ones((2,2))
    #make target grid as a dictionary, for convenience
    target_grid_dict = {}
    target_grid_list = []
    idx = 0
    li = 2       #size of the target grid

    for i in range (li):
        for j in range (li):
            target_grid_dict[idx] = [i*ratio,j*ratio]
            target_grid_list.append(idx)
            idx+=1

    for it in range(1,len(Nk0)):

        if it==nit: break

        Npixels = Nk0[it] - Nk0[it-1]     #new pixels to generate in this iteration
        #select areas where pixels will be thrown
        area_choices = rnd.choice(target_grid_list,Nk0[it], replace = False)

        #remove pixelless sections
        for item in target_grid_list:
            if item not in area_choices:
                target_grid_dict.pop(item)
        
        dict0 = target_grid_dict  #save old dict.
        #update target grid
        dict1 = {}  
        count = 0
        new_ratio = int(ratio/2)    #uptade ratio
        li *= 2       #update target area box size 

        for ii in range (li):
            for jj in range(li):
                for item in target_grid_dict:
                    if ii*new_ratio >= target_grid_dict[item][0] and ii*new_ratio< target_grid_dict[item][0] + ratio and jj*new_ratio >= target_grid_dict[item][1] and jj*new_ratio< target_grid_dict[item][1] + ratio:
                        dict1.update({count: [ii*new_ratio,jj*new_ratio]})
                        count += 1
        target_grid_list = [i for i in range(count)]   #actual list to choose from
        target_grid_dict = dict1
        ratio = new_ratio       #update ratio

    # get inverse grid dict. Will be useful later    
    inv_target_grid_dict = get_inv_dict(target_grid_dict)
    grid = np.zeros((l,l))  #initialize grid
    
    #place empty pixels first if available
    if null_px_ratio is not None:
        null_px_dict = {}
        null_px_list = []
        nn = 0
        for x in range(l):
            for y in range(l):
                null_px_dict[nn] = [x,y]
                null_px_list.append(nn)
                nn+=1
        null_pixels = rnd.choice(null_px_list, null_pixels)
        for null_pixel in null_pixels:
            grid[null_px_dict[null_pixel][0], null_px_dict[null_pixel][1]] = 2
            # try:
            #     if (inv_target_grid_dict[null_px_dict[null_pixel][0], null_px_dict[null_pixel][1]] in target_grid_list):
            #         target_grid_list.remove(null_pixel)    #remove from pool choices
            # except: pass


    #place one pixel in each box of the Nth-1 boxes
    for item in dict0:
        dum_dict = {}
        dum_list = []
        count = 0
        for i in range (2):
            for j in range(2):
                dum_dict[count] = [dict0[item][0]+i , dict0[item][1]+j]
                dum_list.append(count)
                count+=1
        pixel = rnd.choice(dum_list)

        grid[dum_dict[pixel][0] , dum_dict[pixel][1]] = 1   #place pixel

        #use inv dict to know wich pixel hast been selected already
        sel_px = inv_target_grid_dict[dum_dict[pixel][0] , dum_dict [pixel][1]]
        target_grid_list.remove(sel_px )    #remove from pool choices

    # place the rest of the pixels
    pixel_choices = rnd.choice(target_grid_list,Nk0[-1] - Nk0[-2], replace = False)
    for pixel in pixel_choices:
        grid[target_grid_dict[pixel][0],target_grid_dict[pixel][1]] = 1


    if graph_name is not None:
        fig = plt.figure(figsize=(8,6))
        ax = sns.heatmap( grid ,cmap = 'coolwarm' )
        ax.set(xticklabels=[])
        ax.set(yticklabels=[])
    #        plt.pcolormesh(border_grid)
        fig.savefig(graph_name+".png", dpi=200)

    count = 0
    for i in range(l):
        for j in range(l):
            if grid[i,j]==1:
                count+=1

    return grid, dim_change


#----------------------------------------------------------------------------------------------


class Scheling:

    def __init__(self, grid, exp_path = "tmp/p24"):

        self.min_dict = {}
        self.apathetic_dict = {}
        self.maj_dict = {}

        self.min_list = []
        self.maj_list = []
        self.apathetic_list = []


        self.l = grid.shape[0]

        tt = 0 
        for i in range (self.l):
            for j in range(self.l):
                if grid[i,j] == 0:
                    self.apathetic_dict[tt] = [i,j]
                    self.maj_list.append(tt)
                elif grid[i,j] == 1:
                    self.min_dict[tt] = [i,j]
                    self.min_list.append(tt)
                elif grid[i,j] == 2:
                    self.maj_dict[tt] = [i,j]
                    self.apathetic_list.append(tt)
                else: quit("hay un valor que no es 0, 1 o 2")
                tt+=1
        #obtain neighbours dictionary
        self.neigh_dict = self.get_neighbours()

        self.uns_dict = {}
        self.uns_list = []

        self.exp_path = exp_path
#----------------------------------------------------------------------------------------------

    def ComputeDim(self,grid):

        m = int(np.log2(self.l))

        ms = [i for i in range(m+1)]
        mexps = [2**i for i in range(m+1)]
        counts = []

        for gridsize in mexps:
            ratio = int(self.l / gridsize)
            ii=0; jj=0
            count = 0
            for nx in range(gridsize):
                for ny in range(gridsize):
                    box = False
                    for i in range(ratio):
                        for j in range(ratio):
                            if grid[nx*ratio+i,ny*ratio+j] == 1:
                                box = True
                                break
                            if box == True: break

                    if box == True: count+=1
            counts.append(count)
        
        klists = [2**(-k) for k in range(m+1)]
        result = linregress(-np.log(klists), np.log(counts))


        return result[0]

#----------------------------------------------------------------------------------------------
    def get_neighbours(self):

        '''
        map grid points to their respective neighbours  
        '''

        neigh_dict = {}
        for i in range(1,self.l-1):
            for j in range(1,self.l-1):
                neighx = [i-1,i,i+1]
                neighy = [j-1,j,j+1]
                dum_list = []
                for nx, ny in product(neighx,neighy):
                    if nx!=i or ny!=j: 
                        dum_list.append([nx,ny])
                neigh_dict[(i,j)] = dum_list

        #check lower horizontal border
        for i in range(1,self.l-1):
            neighx = [i-1,i,i+1]
            neighy = [0,1]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if nx!=i or ny!=0:
                    dum_list.append([nx,ny])
            neigh_dict[(i,0)] = dum_list

        #check upper horizontal border
        for i in range(1,self.l-1):
            neighx = [i-1,i,i+1]
            neighy = [self.l-2,self.l-1]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if nx!=i or ny!=self.l-1:
                    dum_list.append([nx,ny])
            neigh_dict[(i,self.l-1)] = dum_list

        #check left vertical border
        for j in range(1,self.l-1):
            neighx = [1]
            neighy = [j-1,j,j+1]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if nx!=0 or ny!=j:
                    dum_list.append([nx,ny])
            neigh_dict[(0,j)] = dum_list

        #check right vertical border
        for j in range(1,self.l-1):
            neighx = [self.l-2, self.l-1]
            neighy = [j-1,j,j+1]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if nx!=self.l-1 or ny!=j:
                    dum_list.append([nx,ny])
            neigh_dict[(self.l-1,j)] = dum_list

        #check lower left corner
        dum_list = []
        for nx, ny in zip([0,1,1],[1,0,1]):
            dum_list.append([nx,ny])
            neigh_dict[(0,0)] = dum_list

        #check lower left corner
        dum_list = []
        for nx, ny in zip([self.l-2,self.l-2,self.l-1],[0,1,1]):
            dum_list.append([nx,ny])
            neigh_dict[(self.l-1,0)] = dum_list

        #check upper left corner
        dum_list = []
        for nx, ny in zip([1,1,0],[self.l-1,self.l-2,self.l-2]):
            dum_list.append([nx,ny])
            neigh_dict[(0,self.l-1)] = dum_list

        #check upper right corner
        dum_list = []
        for nx, ny in zip([self.l-2,self.l-2,self.l-1],[self.l-1,self.l-2,self.l-2]):
            dum_list.append([nx,ny])
            neigh_dict[(self.l-1,self.l-1)] = dum_list

        return neigh_dict
#----------------------------------------------------------------------------------------------

    def get_border(self, grid, plot_fig = False):

        border_grid = np.zeros((self.l,self.l))
        for i in range(self.l):
            for j in range(self.l):
                if grid[i,j]==1:
                    for neigh in self.neigh_dict[i,j]:
                        if  grid[neigh[0],neigh[1]] != 1:
                            border_grid[i,j] = 1
                            break

        if plot_fig == True:
            fig = plt.figure(figsize=(8,6))
            ax = sns.heatmap( border_grid ,cmap = 'coolwarm' )
            ax.set(xticklabels=[])
            ax.set(yticklabels=[])
    #        plt.pcolormesh(border_grid)
            fig.savefig(self.exp_path+"/border.png", dpi=200)


        # if graph_name is not None:
        #     plt.clf()   
        #     plt.pcolormesh(border_grid)
        #     plt.savefig("patron_borde")

        return border_grid

#----------------------------------------------------------------------------------------------

    def get_unsatisfied(self, grid, plot = False):

        self.uns_dict = {}
        self.uns_list = []

        uns_grid = np.zeros((self.l,self.l))
        idx = 0
        for i in range(self.l):
            for j in range(self.l):
                if grid[i,j] != 2:
                    count_min, count_maj = self.count_neigh_state([i,j], grid)

                    if count_min +1 < count_maj and grid[i,j] == 1:
                        uns_grid[i,j] = 1

                        self.uns_list.append(idx)
                        self.uns_dict[idx] = [i,j]
                        idx += 1
                        
                    if count_maj +1 < count_min and grid[i,j] == 0:
                        uns_grid[i,j] = 1

                        self.uns_list.append(idx)
                        self.uns_dict[idx] = [i,j]
                        idx += 1

        if plot is True:                    
            fig = plt.figure(figsize=(8,6))
            ax = sns.heatmap( uns_grid ,cmap = 'coolwarm' )
            ax.set(xticklabels=[])
            ax.set(yticklabels=[])
    #        plt.pcolormesh(border_grid)
            fig.savefig(self.exp_path+"/unsatisfied.png", dpi=200)


        return #uns_grid

#---------------------------------------------------------------------------------------------
    def count_neigh_state(self, point, grid):

        count_min = 0
        count_maj = 0
        for neigh in self.neigh_dict[ point[0],point[1] ]:
            if  grid[neigh[0],neigh[1]] == 1:
                count_min += 1 
            elif grid[neigh[0],neigh[1]] == 0:
                count_maj += 1

        return count_min, count_maj
#----------------------------------------------------------------------------------------------    


    def step(self,grid):

        uns_lst_cp = self.uns_list.copy()
        empty_lst_cp = self.apathetic_list.copy()

        condition = False
        while condition is False:

            try:
                empty_point = rnd.choice(empty_lst_cp, replace = False)
                empty_lst_cp.remove(empty_point)
                count_min_emp, count_maj_emp = self.count_neigh_state(self.maj_dict[empty_point], grid)

                uns_point = rnd.choice(uns_lst_cp, replace = False)
                uns_lst_cp.remove(uns_point)
                count_min_uns, count_maj_uns = self.count_neigh_state(self.uns_dict[uns_point], grid)

                if grid[self.uns_dict[uns_point][0] , self.uns_dict[uns_point][1]] == 1:
                    count_emp = count_min_emp
                    count_uns = count_min_uns
                elif grid[self.uns_dict[uns_point][0] , self.uns_dict[uns_point][1]] == 0:
                    count_emp = count_maj_emp
                    count_uns = count_maj_uns

                if count_uns < count_emp:
                    condition = True

                    empty_grid_value = int(np.copy(grid[self.maj_dict[empty_point][0] , self.maj_dict[empty_point][1]]))
                    uns_grid_value = int(np.copy(grid[self.uns_dict[uns_point][0] , self.uns_dict[uns_point][1]]))

                    #swich points
                    grid[self.maj_dict[empty_point][0] , self.maj_dict[empty_point][1]] = uns_grid_value
                    grid[self.uns_dict[uns_point][0] , self.uns_dict[uns_point][1]] = empty_grid_value
            except: 
                break

        return grid    



#-----------------------------------------------------------------------------------------------------------------------------------
class Sznajd:

    def __init__(self, grid, exp_path = "tmp/p23"):

        self.min_dict = {}
        self.maj_dict = {}
        self.apathetic_dict = {}

        self.min_list = []
        self.maj_list = []
        self.apathetic_list = []

        self.l = grid.shape[0]

        tt = 0 
        for i in range (self.l):
            for j in range(self.l):
                if grid[i,j] == 0:
                    self.apathetic_dict[tt] = [i,j]
                    self.apathetic_list.append(tt)
                elif grid[i,j] == 1:
                    self.min_dict[tt] = [i,j]
                    self.min_list.append(tt)

                elif grid[i,j] == -1:
                    self.maj_dict[tt] = [i,j]
                    self.maj_list.append(tt)
                else: quit("hay un valor que no es 0, 1 o 2")
                tt+=1

        self.non_apatethic_dict = self.min_dict | self.maj_dict
        self.non_apatethic_list = self.min_list + self.maj_list
        self.exp_path = exp_path

        self.second_neighs = self.SecondNeighs(grid)

#---------------------------------------------------------------
    def SecondNeighs(self,grid):

        #all points in the center
        neigh_dict = {}
        for i in range(2,self.l-2):
            for j in range(2,self.l-2):
                if grid[i,j] !=0:
                    neighx = [i-2,i-1,i,i+1,i+2]
                    neighy = [j-2,j-1,j,j+1,j+2]
                    dum_list = []
                    for nx, ny in product(neighx,neighy):
                        if (nx!=i or ny!=j) and grid[nx,ny]!=0 and (abs(i-nx)+abs(j-ny)==2):
                            dum_list.append([nx,ny])
                    neigh_dict[(i,j)] = dum_list

        #point in the inward left border:
        for j in range(2,self.l-2):
            if grid[1,j] !=0:
                neighx = [0,1,2,3]
                neighy = [j-2,j-1,j,j+1,j+2]
                dum_list = []
                for nx, ny in product(neighx,neighy):
                    if (nx!=1 or ny!=j) and grid[nx,ny]!=0 and (abs(nx-1)+abs(ny-j)==2):
                        dum_list.append([nx,ny])
                neigh_dict[(1,j)] = dum_list

        #point in the outward left border:
        for j in range(2,self.l-2):
            if grid[0,j] !=0:
                neighx = [0,1,2]
                neighy = [j-2,j-1,j,j+1,j+2]
                dum_list = []
                for nx, ny in product(neighx,neighy):
                    if (nx!=0 or ny!=j) and grid[nx,ny]!=0 and (abs(nx)+abs(ny-j)==2):
                        dum_list.append([nx,ny])
                neigh_dict[(0,j)] = dum_list

        #point in the inward right border:
        for j in range(2,self.l-2):
            if grid[self.l-2,j] !=0:
                neighx = [self.l-4,self.l-3,self.l-2,self.l-1]
                neighy = [j-2,j-1,j,j+1,j+2]
                dum_list = []
                for nx, ny in product(neighx,neighy):
                    if (nx!=self.l-2 or ny!=j) and grid[nx,ny]!=0 and (abs(nx-self.l+2)+abs(ny-j)==2):
                        dum_list.append([nx,ny])
                neigh_dict[(self.l-2,j)] = dum_list

        #point in the outward right border:
        for j in range(2,self.l-2):
            if grid[self.l-1,j] !=0:
                neighx = [self.l-3,self.l-2,self.l-1]
                neighy = [j-2,j-1,j,j+1,j+2]
                dum_list = []
                for nx, ny in product(neighx,neighy):
                    if (nx!=self.l-1 or ny!=j) and grid[nx,ny]!=0 and (abs(nx-self.l+1)+abs(ny-j)==2):
                        dum_list.append([nx,ny])
                neigh_dict[(self.l-1,j)] = dum_list

        #point in the inward lower border:
        for i in range(2,self.l-2):
            if grid[i,1] !=0:
                neighx = [i-2,i-1,i,i+1,i+2]
                neighy = [0,1,2,3]
                dum_list = []
                for nx, ny in product(neighx,neighy):
                    if (nx!=i or ny!=1) and grid[nx,ny]!=0 and (abs(nx-i)+abs(ny-1)==2):
                        dum_list.append([nx,ny])
                neigh_dict[(i,1)] = dum_list

        #point in the outward lower border:
        for i in range(2,self.l-2):
            if grid[i,0] !=0:
                neighx = [i-2,i-1,i,i+1,i+2]
                neighy = [0,1,2]
                dum_list = []
                for nx, ny in product(neighx,neighy):
                    if (nx!=i or ny!=0) and grid[nx,ny]!=0 and (abs(nx-i)+abs(ny)==2):
                        dum_list.append([nx,ny])
                neigh_dict[(i,0)] = dum_list

        #point in the inward upper border:
        for i in range(2,self.l-2):
            if grid[i,self.l-2] !=0:
                neighx = [i-2,i-1,i,i+1,i+2]
                neighy = [self.l-4,self.l-3,self.l-2,self.l-1]
                dum_list = []
                for nx, ny in product(neighx,neighy):
                    if (nx!=i or ny!=self.l-2) and grid[nx,ny]!=0 and (abs(nx-i)+abs(ny-self.l+2)==2):
                        dum_list.append([nx,ny])
                neigh_dict[(i,self.l-2)] = dum_list

        #point in the outward upper border:
        for i in range(2,self.l-2):
            if grid[i,self.l-1] !=0:
                neighx = [i-2,i-1,i,i+1,i+2]
                neighy = [self.l-3,self.l-2,self.l-1]
                dum_list = []
                for nx, ny in product(neighx,neighy):
                    if (nx!=i or ny!=self.l-1) and grid[nx,ny]!=0 and (abs(nx-i)+abs(ny-self.l+1)==2):
                        dum_list.append([nx,ny])
                neigh_dict[(i,self.l-1)] = dum_list


        #points in the lower left corner:
        if grid[0,0]!=0:
            neighx = [0,1,2]
            neighy = [0,1,2]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=0 or ny!=0) and grid[nx,ny]!=0 and (abs(nx)+abs(ny)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(0,0)] = dum_list

        if grid[1,0]!=0:
            neighx = [0,1,2,3]
            neighy = [0,1,2]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=1 or ny!=0) and grid[nx,ny]!=0 and (abs(nx-1)+abs(ny)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(1,0)] = dum_list

        if grid[0,1]!=0:
            neighx = [0,1,2]
            neighy = [0,1,2,3]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=0 or ny!=1) and grid[nx,ny]!=0 and (abs(nx)+abs(ny-1)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(0,1)] = dum_list

        if grid[1,1]!=0:
            neighx = [0,1,2,3]
            neighy = [0,1,2,3]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=1 or ny!=1) and grid[nx,ny]!=0 and (abs(nx-1)+abs(ny-1)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(1,1)] = dum_list


        if grid[0,self.l-1]!=0:
            #points in the upper left corner:
            neighx = [0,1,2]
            neighy = [self.l-1,self.l-2,self.l-3]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=0 or ny!=self.l-1) and grid[nx,ny]!=0 and (abs(nx)+abs(ny-self.l+1)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(0,self.l-1)] = dum_list

        if grid[1,self.l-1]!=0:
            neighx = [0,1,2,3]
            neighy = [self.l-1,self.l-2,self.l-3]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=1 or ny!=self.l-1) and grid[nx,ny]!=0 and (abs(nx-1)+abs(ny-self.l+1)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(1,self.l-1)] = dum_list

        if grid[0,self.l-2]!=0:
            neighx = [0,1,2]
            neighy = [self.l-1,self.l-2,self.l-3,self.l-4]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=0 or ny!=self.l-2) and grid[nx,ny]!=0 and (abs(nx)+abs(ny-self.l+2)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(0,self.l-2)] = dum_list

        if grid[1,self.l-2] !=0:
            neighx = [0,1,2,3]
            neighy = [self.l-1,self.l-2,self.l-3,self.l-4]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=1 or ny!=self.l-2) and grid[nx,ny]!=0 and (abs(nx-1)+abs(ny-self.l+2)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(1,self.l-2)] = dum_list


        if grid[self.l-1,0]!=0:
            #points in the lower right corner:
            neighx = [self.l-1,self.l-2,self.l-3]
            neighy = [0,1,2]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=self.l-1 or ny!=0) and grid[nx,ny]!=0 and (abs(nx-self.l+1)+abs(ny)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(self.l-1,0)] = dum_list

        if grid[self.l-2,0]!=0:
            neighx = [self.l-1,self.l-2,self.l-3,self.l-4]
            neighy = [0,1,2]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=self.l-2 or ny!=0) and grid[nx,ny]!=0 and (abs(nx-self.l+2)+abs(ny)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(self.l-2,0)] = dum_list

        if grid[self.l-1,1]!=0:
            neighx = [self.l-1,self.l-2,self.l-3]
            neighy = [0,1,2,3]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=self.l-1 or ny!=1) and grid[nx,ny]!=0 and (abs(nx-self.l+1)+abs(ny-1)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(self.l-1,1)] = dum_list

        if  grid[self.l-2,1]!=0:
            neighx = [self.l-1,self.l-2,self.l-3,self.l-4]
            neighy = [0,1,2,3]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=self.l-2 or ny!=1) and grid[nx,ny]!=0 and (abs(nx-self.l+2)+abs(ny-1)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(self.l-2,1)] = dum_list

        if  grid[self.l-1,self.l-1]!=0:
            #points in the upper right corner:
            neighx = [self.l-1,self.l-2,self.l-3]
            neighy = [self.l-1,self.l-2,self.l-3]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=self.l-1 or ny!=self.l-1) and grid[nx,ny]!=0 and (abs(nx-self.l+1)+abs(ny-self.l+1)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(self.l-1,self.l-1)] = dum_list

        if  grid[self.l-2,self.l-1]!=0:
            neighx = [self.l-1,self.l-2,self.l-3,self.l-4]
            neighy = [self.l-1,self.l-2,self.l-3]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=self.l-2 or ny!=self.l-1) and grid[nx,ny]!=0 and (abs(nx-self.l+2)+abs(ny-self.l+1)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(self.l-2,self.l-1)] = dum_list

        if grid[self.l-1,self.l-2]!=0:
            neighx = [self.l-1,self.l-2,self.l-3]
            neighy = [self.l-1,self.l-2,self.l-3,self.l-4]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=self.l-1 or ny!=self.l-2) and grid[nx,ny]!=0 and (abs(nx-self.l+1)+abs(ny-self.l+2)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(self.l-1,self.l-2)] = dum_list

        if  grid[self.l-2,self.l-2]!=0:
            neighx = [self.l-1,self.l-2,self.l-3,self.l-4]
            neighy = [self.l-1,self.l-2,self.l-3,self.l-4]
            dum_list = []
            for nx, ny in product(neighx,neighy):
                if (nx!=self.l-2 or ny!=self.l-2) and grid[nx,ny]!=0 and (abs(nx-self.l+2)+abs(ny-self.l+2)==2):
                    dum_list.append([nx,ny])
            neigh_dict[(self.l-2,self.l-2)] = dum_list

        return neigh_dict
#---------------------------------------------------------------

#def SecondNeighs(self,grid):

        ##all points in the center
        #neigh_dict = {}
        #for i in range(2,self.l-2):
            #for j in range(2,self.l-2):
                #if grid[i,j] !=0:
                    #neighx = [i-2,i-1,i,i+1,i+2]
                    #neighy = [j-2,j-1,j,j+1,j+2]
                    #dum_list = []
                    #for nx, ny in product(neighx,neighy):
                        #if (nx!=i or ny!=j) and grid[nx,ny]!=0:
                            #dum_list.append([nx,ny])
                    #neigh_dict[(i,j)] = dum_list

        ##point in the inward left border:
        #for j in range(2,self.l-2):
            #if grid[1,j] !=0:
                #neighx = [0,1,2,3]
                #neighy = [j-2,j-1,j,j+1,j+2]
                #dum_list = []
                #for nx, ny in product(neighx,neighy):
                    #if (nx!=1 or ny!=j) and grid[nx,ny]!=0:
                        #dum_list.append([nx,ny])
                #neigh_dict[(1,j)] = dum_list

        ##point in the outward left border:
        #for j in range(2,self.l-2):
            #if grid[0,j] !=0:
                #neighx = [0,1,2]
                #neighy = [j-2,j-1,j,j+1,j+2]
                #dum_list = []
                #for nx, ny in product(neighx,neighy):
                    #if (nx!=0 or ny!=j) and grid[nx,ny]!=0:
                        #dum_list.append([nx,ny])
                #neigh_dict[(0,j)] = dum_list

        ##point in the inward right border:
        #for j in range(2,self.l-2):
            #if grid[self.l-2,j] !=0:
                #neighx = [self.l-4,self.l-3,self.l-2,self.l-1]
                #neighy = [j-2,j-1,j,j+1,j+2]
                #dum_list = []
                #for nx, ny in product(neighx,neighy):
                    #if (nx!=self.l-2 or ny!=j) and grid[nx,ny]!=0:
                        #dum_list.append([nx,ny])
                #neigh_dict[(self.l-2,j)] = dum_list

        ##point in the outward right border:
        #for j in range(2,self.l-2):
            #if grid[self.l-1,j] !=0:
                #neighx = [self.l-3,self.l-2,self.l-1]
                #neighy = [j-2,j-1,j,j+1,j+2]
                #dum_list = []
                #for nx, ny in product(neighx,neighy):
                    #if (nx!=self.l-1 or ny!=j) and grid[nx,ny]!=0:
                        #dum_list.append([nx,ny])
                #neigh_dict[(self.l-1,j)] = dum_list

        ##point in the inward lower border:
        #for i in range(2,self.l-2):
            #if grid[i,1] !=0:
                #neighx = [i-2,i-1,i,i+1,i+2]
                #neighy = [0,1,2,3]
                #dum_list = []
                #for nx, ny in product(neighx,neighy):
                    #if (nx!=i or ny!=1) and grid[nx,ny]!=0:
                        #dum_list.append([nx,ny])
                #neigh_dict[(i,1)] = dum_list

        ##point in the outward lower border:
        #for i in range(2,self.l-2):
            #if grid[i,0] !=0:
                #neighx = [i-2,i-1,i,i+1,i+2]
                #neighy = [0,1,2]
                #dum_list = []
                #for nx, ny in product(neighx,neighy):
                    #if (nx!=i or ny!=0) and grid[nx,ny]!=0:
                        #dum_list.append([nx,ny])
                #neigh_dict[(i,0)] = dum_list

        ##point in the inward upper border:
        #for i in range(2,self.l-2):
            #if grid[i,self.l-2] !=0:
                #neighx = [i-2,i-1,i,i+1,i+2]
                #neighy = [self.l-4,self.l-3,self.l-2,self.l-1]
                #dum_list = []
                #for nx, ny in product(neighx,neighy):
                    #if (nx!=i or ny!=self.l-2) and grid[nx,ny]!=0:
                        #dum_list.append([nx,ny])
                #neigh_dict[(i,self.l-2)] = dum_list

        ##point in the outward upper border:
        #for i in range(2,self.l-2):
            #if grid[i,self.l-1] !=0:
                #neighx = [i-2,i-1,i,i+1,i+2]
                #neighy = [self.l-3,self.l-2,self.l-1]
                #dum_list = []
                #for nx, ny in product(neighx,neighy):
                    #if (nx!=i or ny!=self.l-1) and grid[nx,ny]!=0:
                        #dum_list.append([nx,ny])
                #neigh_dict[(i,self.l-1)] = dum_list


        ##points in the lower left corner:
        #if grid[0,0]!=0:
            #neighx = [0,1,2]
            #neighy = [0,1,2]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=0 or ny!=0) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(0,0)] = dum_list

        #if grid[1,0]!=0:
            #neighx = [0,1,2,3]
            #neighy = [0,1,2]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=1 or ny!=0) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(1,0)] = dum_list

        #if grid[0,1]!=0:
            #neighx = [0,1,2]
            #neighy = [0,1,2,3]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=0 or ny!=1) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(0,1)] = dum_list

        #if grid[1,1]!=0:
            #neighx = [0,1,2,3]
            #neighy = [0,1,2,3]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=1 or ny!=1) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(1,1)] = dum_list


        #if grid[0,self.l-1]!=0:
            ##points in the upper left corner:
            #neighx = [0,1,2]
            #neighy = [self.l-1,self.l-2,self.l-3]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=0 or ny!=self.l-1) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(0,self.l-1)] = dum_list

        #if grid[1,self.l-1]!=0:
            #neighx = [0,1,2,3]
            #neighy = [self.l-1,self.l-2,self.l-3]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=1 or ny!=self.l-1) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(1,self.l-1)] = dum_list

        #if grid[0,self.l-2]!=0:
            #neighx = [0,1,2]
            #neighy = [self.l-1,self.l-2,self.l-3,self.l-4]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=0 or ny!=self.l-2) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(0,self.l-2)] = dum_list

        #if grid[1,self.l-2] !=0:
            #neighx = [0,1,2,3]
            #neighy = [self.l-1,self.l-2,self.l-3,self.l-4]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=1 or ny!=self.l-2) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(1,self.l-2)] = dum_list


        #if grid[self.l-1,0]!=0:
            ##points in the lower right corner:
            #neighx = [self.l-1,self.l-2,self.l-3]
            #neighy = [0,1,2]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=self.l-1 or ny!=0) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(self.l-1,0)] = dum_list

        #if grid[self.l-2,0]!=0:
            #neighx = [self.l-1,self.l-2,self.l-3,self.l-4]
            #neighy = [0,1,2]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=self.l-2 or ny!=0) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(self.l-2,0)] = dum_list

        #if grid[self.l-1,1]!=0:
            #neighx = [self.l-1,self.l-2,self.l-3]
            #neighy = [0,1,2,3]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=self.l-1 or ny!=1) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(self.l-1,1)] = dum_list

        #if  grid[self.l-2,1]!=0:
            #neighx = [self.l-1,self.l-2,self.l-3,self.l-4]
            #neighy = [0,1,2,3]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=self.l-2 or ny!=1) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(self.l-2,1)] = dum_list

        #if  grid[self.l-1,self.l-1]!=0:
            ##points in the upper right corner:
            #neighx = [self.l-1,self.l-2,self.l-3]
            #neighy = [self.l-1,self.l-2,self.l-3]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=self.l-1 or ny!=self.l-1) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(self.l-1,self.l-1)] = dum_list

        #if  grid[self.l-2,self.l-1]!=0:
            #neighx = [self.l-1,self.l-2,self.l-3,self.l-4]
            #neighy = [self.l-1,self.l-2,self.l-3]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=self.l-2 or ny!=self.l-1) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(self.l-2,self.l-1)] = dum_list

        #if grid[self.l-1,self.l-2]!=0:
            #neighx = [self.l-1,self.l-2,self.l-3]
            #neighy = [self.l-1,self.l-2,self.l-3,self.l-4]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=self.l-1 or ny!=self.l-2) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(self.l-1,self.l-2)] = dum_list

        #if  grid[self.l-2,self.l-2]!=0:
            #neighx = [self.l-1,self.l-2,self.l-3,self.l-4]
            #neighy = [self.l-1,self.l-2,self.l-3,self.l-4]
            #dum_list = []
            #for nx, ny in product(neighx,neighy):
                #if (nx!=self.l-2 or ny!=self.l-2) and grid[nx,ny]!=0:
                    #dum_list.append([nx,ny])
            #neigh_dict[(self.l-2,self.l-2)] = dum_list

        #return neigh_dict
#---------------------------------------------------------------


    def CheckSecondNeigh(self,grid):

        for item in self.non_apatethic_list:
            point = self.non_apatethic_dict[item]
            grid[point[0],point[1]] = 1


        for item in self.non_apatethic_list:
            grid1 = np.copy(grid)
            point = self.non_apatethic_dict[item]
            neighs = self.second_neighs[(point[0],point[1])]

            grid1[point[0],point[1]]=2
            for neigh in neighs:
                grid1[neigh[0],neigh[1]] = 3

            fig = plt.figure(figsize=(8,6))
            ax = sns.heatmap( grid1 ,cmap = 'coolwarm' )
            ax.set(xticklabels=[])
            ax.set(yticklabels=[])
            fig.savefig(self.exp_path+"/control/control_plot_"+str(point[0])+str(point[1])+".png", dpi=200)


#---------------------------------------------------------------

    def SznajdStep(self, grid):
        #choose non-apathetic point
        condition = False
        while condition is False:
            choice = rnd.choice(self.non_apatethic_list)
            point = self.non_apatethic_dict[choice]
            #choose random second neighbour
            neighs_list = self.second_neighs[point[0],point[1]]
            choices_list = [i for i in range(len(neighs_list))]
            if len(neighs_list)!=0:
                condition = True

        neig = neighs_list[rnd.choice(choices_list)]

        #update lists
        if grid[point[0],point[1]] !=  grid[neig[0],neig[1]]:
            grid[point[0],point[1]] = -grid[point[0],point[1]]

        return grid
#---------------------------------------------------------------

    def isgreen2(self,grid, point):

        i,j = point

        apa=0
        test=[grid[i+1,j]!=grid[i,j],grid[i,j+1]!=grid[i,j],grid[i-1,j]!=grid[i,j],grid[i,j-1]!=grid[i,j]]
        if grid[i+1,j]==-1:
            apa+=1
        if grid[i,j+1]==-1:
            apa+=1
        if grid[i-1,j]==-1:
            apa+=1
        if grid[i,j-1]==-1:
            apa+=1
        if apa<3:
            #si tengo 2 o menos vecinos apaticos
            if sum(test)>3: #y los vecinos son como deben
                return True
            else:
                return False
        else: return False

#---------------------------------------------------------------

    def Count(self, grid):
        red = 0
        blue = 0
        for item in self.non_apatethic_list:
            point = self.non_apatethic_dict[item]
            if grid[point[0],point[1]] == 1:
                red += 1
            if grid[point[0],point[1]] == -1:
                blue += 1
        return red, blue
#---------------------------------------------------------------
