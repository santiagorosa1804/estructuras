import numpy as np
import matplotlib.pyplot as plt
import patron2d
import os
import seaborn as sns
from scipy.stats import linregress

#------------------------------------------------------------------------------
def ComputeDim(grid):

        #compute the BC dimension of a 2d pattern

        l = grid.shape[0]
        m = int(np.log2(l))

        ms = [i for i in range(m+1)]
        mexps = [2**i for i in range(m+1)]
        counts = []

        for gridsize in mexps:
            ratio = int(l / gridsize)
            ii=0; jj=0
            count = 0
            for nx in range(gridsize):
                for ny in range(gridsize):
                    box = False
                    for i in range(ratio):
                        for j in range(ratio):
                            if grid[nx*ratio+i,ny*ratio+j] == 1:
                                box = True
                                break
                            if box == True: break

                    if box == True: count+=1
            counts.append(count)

        klists = [2**(-k) for k in range(m+1)]
        result = linregress(-np.log(klists), np.log(counts))


        return result[0]
#------------------------------------------------------------------------------


exp_folder = "tmp/p15"
try: os.mkdir(exp_folder)
except: pass

l = 64
r = .3
D = 1.65
D_epsilon = 0.02
m = np.log2(l/2.)

grid, _ = patron2d.Pattern2D(r,l,D,D_epsilon)

print(ComputeDim(grid))

fig = plt.figure(figsize=(8,6))
ax = sns.heatmap( grid ,cmap = 'coolwarm' )
ax.set(xticklabels=[])
ax.set(yticklabels=[])
fig.savefig(exp_folder+"/grid.png", dpi=200)

